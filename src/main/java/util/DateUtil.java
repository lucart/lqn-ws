package util;

import java.util.Date;

public final class DateUtil {
	public static int daysBetween(Date d1, Date d2){
        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
	}
	
	public static long getUnixTimeStampFromDate(Date date){
		return date.getTime() / 1000;
	}
	
	public static Date getDateFromUnixTimeStamp(long timestamp){
		return new Date((long)timestamp*1000);
	}
	
	public static double roundTwoDecimalsDouble(double value){
		return Math.round(value * 100.0) / 100.0;
	}
}
