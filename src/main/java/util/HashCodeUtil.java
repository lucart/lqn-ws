package util;

import java.util.Random;

public final class HashCodeUtil {

    public static String getHashCode() {
        String part1 = getRandomStringByChar(3, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        String part2 = getRandomStringByChar(3, "1234567890");
        return part1 + part2;
    }

    private static String getRandomStringByChar(int length, String chars) {
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) {
            int index = (int) (rnd.nextFloat() * chars.length());
            salt.append(chars.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }
}
