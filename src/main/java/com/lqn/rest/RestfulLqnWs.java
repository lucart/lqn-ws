package com.lqn.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.lqn.exception.InvalidPageException;
import com.lqn.model.Offer;
import com.lqn.model.OfferUser;
import com.lqn.model.User;
import com.lqn.requests.OfferRequest;
import com.lqn.responses.*;
import com.lqn.service.LoginService;
import com.lqn.service.OfferService;
import com.lqn.service.OfferUserService;
import com.lqn.service.UserService;
import org.apache.log4j.Logger;


@Path("/")
public class RestfulLqnWs 
{
	final static Logger log = Logger.getLogger(RestfulLqnWs.class);

    /**
     * Get Json Offer List by typeId and pageNumber
     * @param pageNumber Page Number
     * @param offerTypeId Offer Type Id
     * @return Json Offer List
     */
	@GET
	@Path("/getOffers/{pageNumber}/{offerTypeId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOffers(@PathParam("pageNumber") int pageNumber,
							  @PathParam("offerTypeId") int offerTypeId){
		try{
		    log.info("getOffers(pageNumber: " + pageNumber + ", offerTypeId: " + offerTypeId + ")");
			OfferService offerService = new OfferService();
			List<Offer> offers = offerService.findByOfferTypeId(pageNumber, offerTypeId);
			List<OfferResponse> offersResponse = offerService.getOfferResponseListFromOffers(offers);
			
			return Response.status(200).entity(offersResponse).build();
		}catch(InvalidPageException e){
		    log.error(e);
			ErrorResponse error = new ErrorResponse(412, e.getMessage());
			return Response.status(401).entity(error).build();
		}
	}

    /**
     * Login with credentials
     * @param username Username
     * @param password Password
     * @return
     */
	@GET
	@Path("/login/{username}/{password}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(@PathParam("username") String username,
						  @PathParam("password") String password){
        log.info("login(username: " + username + ", password: " + password + ")");
		LoginService loginService = new LoginService();
		UserService userService = new UserService();
		User user = loginService.login(username, password);
		UserResponse userResponse = userService.getUserResponseFromUser(user);
		if(user != null){
			return Response.status(200).entity(userResponse).build();
		}else{
			ErrorResponse error = new ErrorResponse(401, "Invalid Username or Password");
			return Response.status(401).entity(error).build();
		}		 
	}

    /**
     * Insert Offer Object
     * @param offerRequest jsonOffer
     * @return status 200
     */
	@POST
	@Path("/insertOffer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response insertOffer(OfferRequest offerRequest){
		try{
            log.info("insertOffer(jsonOffer: " + offerRequest.toString() + ")");
			OfferService offerService = new OfferService();
			offerService.insertOffer(offerRequest);
			
			GenericResponse response = new GenericResponse(200, "Offer Inserted");
			
			return Response.status(200).entity(response).build();
		}catch(Exception e){
		    log.error(e);
			ErrorResponse error = new ErrorResponse(412, e.getMessage());
			return Response.status(401).entity(error).build();
		}	 
	}

    /**
     * Update an Offer
     * @param offerRequest
     * @return
     */
	@POST
	@Path("/updateOffer")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateOffer(OfferRequest offerRequest){
		try{
            log.info("updateOffer(jsonOffer: " + offerRequest.toString() + ")");
			OfferService offerService = new OfferService();
			offerService.updateOffer(offerRequest);
			
			GenericResponse response = new GenericResponse(200, "Offer Updated");
			
			return Response.status(200).entity(response).build();
		}catch(Exception e){
		    log.error(e);
			ErrorResponse error = new ErrorResponse(412, e.getMessage());
			return Response.status(401).entity(error).build();
		}	 
	}

    /**
     * Get Offers By Shop Id
     * @param shopId
     * @return
     */
	@GET
	@Path("/getOffersByShop/{shopId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOffersByShop(@PathParam("shopId") int shopId){
		try{
            log.info("getOffersByShop(shopId: " + shopId + ")");
			OfferService offerService = new OfferService();
			List<Offer> offers = offerService.findOffersByShopId(shopId);
			List<OfferResponseForShop> offersResponse = offerService.getOfferResponseForShopListFromOffers(offers);
			return Response.status(200).entity(offersResponse).build();
		}catch(InvalidPageException e){
		    log.error(e);
			ErrorResponse error = new ErrorResponse(412, e.getMessage());
			return Response.status(401).entity(error).build();
		}
	}

    /**
     * Get Offers By User ID
     * @param userId
     * @return
     */
	@GET
	@Path("/getOffersByUser/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getOffersByUser(@PathParam("userId") int userId){
		try{
            log.info("getOffersByUser(userId: " + userId + ")");
            OfferService offerService = new OfferService();
			OfferUserService offerUserService = new OfferUserService();
			List<OfferUser> offersUser = offerUserService.findOffersUserByUserId(userId);
			List<OfferResponseForUser> offersResponse = offerService.getOfferResponseForUserListFromOffersUser(offersUser);
			return Response.status(200).entity(offersResponse).build();
		}catch(Exception e){
		    log.error(e);
			ErrorResponse error = new ErrorResponse(412, e.getMessage());
			return Response.status(401).entity(error).build();
		}
	}

    /**
     * Delete an Offer
     * @param offerId
     * @return
     */
	@GET
	@Path("/deleteOffer/{offerId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteOffer(@PathParam("offerId") int offerId){
		try{
            log.info("deleteOffer(offerId: " + offerId + ")");
            OfferService offerService = new OfferService();
			offerService.delete(offerId);
			GenericResponse response = new GenericResponse(200, "Offer Deleted");
			return Response.status(200).entity(response).build();
		}catch(Exception e){
		    log.error(e);
			ErrorResponse error = new ErrorResponse(412, e.getMessage());
			return Response.status(401).entity(error).build();
		}
	}

    /**
     * Reserve an Offer
     * @param offerId OfferID to reserve
     * @param userId UserID that wants to reserve the offer
     * @return
     */
	@GET
	@Path("/reserveOffer/{offerId}/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response reserveOffer(@PathParam("offerId") int offerId,
								 @PathParam("userId") int userId){
		try{
            log.info("reserveOffer(offerId: " + offerId + ", userId: " + userId + ")");
            UserService userService = new UserService();
			userService.reserveOffer(offerId, userId);
			GenericResponse response = new GenericResponse(200, "Offer Reserved");
			return Response.status(200).entity(response).build();
		}catch(Exception e){
		    log.error(e);
			ErrorResponse error = new ErrorResponse(412, e.getMessage());
			return Response.status(401).entity(error).build();
		}
	}

    /**
     * Find Offer By HashCode
     * @param hashCode
     * @param userId
     * @return
     */
	@GET
	@Path("/findOfferByHashCode/{hashCode}/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findOfferByHashCode(@PathParam("hashCode") String hashCode,
										@PathParam("userId") int userId){
		try{
            log.info("findOfferByHashCode(hashCode: " + hashCode + ", userId: " + userId + ")");
            OfferUserService offerUserService = new OfferUserService();
			UserService userService = new UserService();
			OfferUser offerUser = offerUserService.findOfferUserByHashCode(hashCode);
			if(userService.isAsociatedToShop(userId, offerUser.getOffer().getShop().getId())) {
				OfferUserResponse offerUserResponse = offerUserService.getOfferUserResponseFromOfferUser(offerUser);
				return Response.status(200).entity(offerUserResponse).build();
			} else {
				ErrorResponse error = new ErrorResponse(412, "access denied");
				return Response.status(403).entity(error).build();
			}
		}catch(Exception e){
		    log.error(e);
			ErrorResponse error = new ErrorResponse(412, e.getMessage());
			return Response.status(401).entity(error).build();
		}
	}

    /**
     * Seasrch Offers by Keys and PageNumber
     * @param pageNumber
     * @param keys
     * @return
     */
    @GET
    @Path("/searchOffers/{pageNumber}/{keys}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchOffers(@PathParam("pageNumber") int pageNumber,
                                 @PathParam("keys") String keys){
        try{
            log.info("searchOffers(pageNumber: " + pageNumber + ", keys: " + keys + ")");
            OfferService offerService = new OfferService();
            List<Offer> offers = offerService.searchOffers(pageNumber, keys);
            List<OfferResponse> offersResponse = offerService.getOfferResponseListFromOffers(offers);

            return Response.status(200).entity(offersResponse).build();
        }catch(InvalidPageException e){
            log.error(e);
            ErrorResponse error = new ErrorResponse(412, e.getMessage());
            return Response.status(401).entity(error).build();
        }
    }

}
