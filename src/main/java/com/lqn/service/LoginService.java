package com.lqn.service;

import com.lqn.model.User;

public class LoginService {
	private static UserService userService;

	public LoginService() {
		userService = new UserService();
	}
	
	public User login(String username, String password){
		User user = userService.findByUsername(username);
		if(user != null){
			if(user.getPassword().equals(password)){
				return user;
			}else{
				return null;
			}			
		}else{
			return null;
		}
	}
}

