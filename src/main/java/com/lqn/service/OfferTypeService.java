package com.lqn.service;

import com.lqn.dao.OfferTypeDaoImpl;
import com.lqn.model.OfferType;

import java.util.List;

public class OfferTypeService {
	private static OfferTypeDaoImpl offerTypeDao;

	public OfferTypeService() {
		offerTypeDao = new OfferTypeDaoImpl();
	}

	public OfferType findById(Integer id) {
		OfferType offerType = offerTypeDao.findById(id);
		return offerType;
	}
}
