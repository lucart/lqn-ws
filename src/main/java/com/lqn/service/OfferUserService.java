package com.lqn.service;

import com.lqn.dao.OfferUserDaoImpl;
import com.lqn.model.OfferUser;
import com.lqn.responses.OfferResponse;
import com.lqn.responses.OfferUserResponse;
import com.lqn.responses.UserResponse;

import java.util.List;

public class OfferUserService {
    private static OfferUserDaoImpl offerUserDao;
    private static OfferService offerService;

    public OfferUserService() {
        offerUserDao = new OfferUserDaoImpl();
        offerService = new OfferService();
    }

    public void persist(OfferUser entity) {
        offerUserDao.persist(entity);
    }

    public OfferUser findOfferUserByHashCode(String hashCode){
        OfferUser offerUser = offerUserDao.findByHashCode(hashCode);
        return offerUser;
    }

    public List<OfferUser> findOffersUserByUserId(int userId){
        List<OfferUser> offersUser = offerUserDao.findByUserId(userId);
        return offersUser;
    }

    public OfferUserResponse getOfferUserResponseFromOfferUser(OfferUser offerUser){
        OfferUserResponse offerUserResponse = new OfferUserResponse();
        OfferResponse offerResponse = offerService.getOfferResponseFromOffer(offerUser.getOffer());

        UserResponse userResponse = new UserResponse();
        userResponse.setId(offerUser.getUser().getId());
        userResponse.setUsername(offerUser.getUser().getUsername());
        userResponse.setUserType(offerUser.getUser().getUserType());

        offerUserResponse.setOffer(offerResponse);
        offerUserResponse.setUser(userResponse);

        return offerUserResponse;
    }

    public boolean validateHashCode(String hashCode){
        return offerUserDao.existHashCode(hashCode);
    }
}
