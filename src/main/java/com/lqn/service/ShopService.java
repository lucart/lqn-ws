package com.lqn.service;

import com.lqn.dao.ShopDaoImpl;
import com.lqn.model.Shop;

public class ShopService {
	private static ShopDaoImpl shopDao;

	public ShopService() {
		shopDao = new ShopDaoImpl();
	}

	public Shop findById(Integer id) {
		Shop shop = shopDao.findById(id);
		return shop;
	}
}
