package com.lqn.service;

import com.lqn.dao.OfferDaoImpl;
import com.lqn.exception.InvalidPageException;
import com.lqn.model.Offer;
import com.lqn.model.OfferUser;
import com.lqn.requests.OfferRequest;
import com.lqn.responses.*;
import util.DateUtil;

import java.util.ArrayList;
import java.util.List;

public class OfferService {
	private static OfferDaoImpl offerDao;
	private static ShopService shopService;
	private static PaymentMethodService paymentMethodService;
	private static OfferTypeService offerTypeService;

	public OfferService() {
		offerDao = new OfferDaoImpl();
		shopService = new ShopService();
		paymentMethodService =  new PaymentMethodService();
		offerTypeService = new OfferTypeService();
	}

	public void persist(Offer entity) {
		offerDao.persist(entity);
	}

	public void update(Offer entity) {
		offerDao.update(entity);
	}

	public Offer findById(Integer id) {
		Offer offer = offerDao.findById(id);
		return offer;
	}

	public void delete(Integer id) {
		Offer offer = offerDao.findById(id);
		offerDao.delete(offer);
	}

	public List<Offer> findAll() {

		List<Offer> offers = offerDao.findAll();
		return offers;
	}
	
	public List<Offer> findByOfferTypeId(int page, int offerTypeId) throws InvalidPageException {
		try{
			List<Offer> offers = offerDao.findByOfferTypeId(page, offerTypeId);
			return offers;
		}catch(Exception e){
			throw new InvalidPageException(e.getMessage());
		}
	}

	public List<Offer> searchOffers(int page, String keys) throws InvalidPageException {
		try{
			List<Offer> offers = offerDao.searchOffers(page, keys);
			return offers;
		}catch(Exception e){
			throw new InvalidPageException(e.getMessage());
		}
	}
	
	public List<Offer> findOffersByShopId(int shopId) throws InvalidPageException {
		try{

			List<Offer> offers = offerDao.findOffersByShopId(shopId);

			return offers;
		}catch(Exception e){
			throw new InvalidPageException(e.getMessage());
		}
	}
	
	public List<OfferResponse> getOfferResponseListFromOffers(List<Offer> offerList){
		List<OfferResponse> offerResponseList = new ArrayList<OfferResponse>();
		for(Offer offer : offerList){
			
			OfferTypeResponse offerTypeResponse = new OfferTypeResponse();
			offerTypeResponse.setId(offer.getOfferType().getId());
			offerTypeResponse.setActualPrice(offer.getActualPrice());
			offerTypeResponse.setPreviousPrice(offer.getPreviousPrice());
			offerTypeResponse.setDiscountPercentage(offer.getDiscountPercentage());
			offerTypeResponse.setComboType(offer.getComboType());
			
			PaymentMethodResponse paymentMethodResponse = new PaymentMethodResponse();
			paymentMethodResponse.setId(offer.getPaymentMethod().getId());
			paymentMethodResponse.setCreditCards(offer.getCreditCards());
			
			OfferResponse offerResponse = new OfferResponse();
			offerResponse.setId(offer.getId());
			offerResponse.setName(offer.getName());
			offerResponse.setDescription(offer.getDescription());
			offerResponse.setExpirationDate(DateUtil.getUnixTimeStampFromDate(offer.getExpirationDate()));
			offerResponse.setImageUrl(offer.getImageUrl());
			offerResponse.setStock(offer.getStock());
			offerResponse.setShop(offer.getShop());
			offerResponse.setOfferType(offerTypeResponse);
			offerResponse.setPaymentMethod(paymentMethodResponse);
			offerResponse.setLqnLoyaltyPoints(offer.getLqnLoyaltyPoints());
			offerResponse.setRefundMoneyPercentage(offer.getRefundMoneyPercentage());
			offerResponse.setEnabled(offer.getEnabled());
			offerResponse.setLqnScore(offer.getLqnScore());
			offerResponseList.add(offerResponse);
		}
		return offerResponseList;
	}

	public List<OfferResponseForShop> getOfferResponseForShopListFromOffers(List<Offer> offerList){
		List<OfferResponseForShop> offerResponseList = new ArrayList<>();
		for(Offer offer : offerList){

			OfferTypeResponse offerTypeResponse = new OfferTypeResponse();
			offerTypeResponse.setId(offer.getOfferType().getId());
			offerTypeResponse.setActualPrice(offer.getActualPrice());
			offerTypeResponse.setPreviousPrice(offer.getPreviousPrice());
			offerTypeResponse.setDiscountPercentage(offer.getDiscountPercentage());
			offerTypeResponse.setComboType(offer.getComboType());

			PaymentMethodResponse paymentMethodResponse = new PaymentMethodResponse();
			paymentMethodResponse.setId(offer.getPaymentMethod().getId());
			paymentMethodResponse.setCreditCards(offer.getCreditCards());

			OfferResponseForShop offerResponse = new OfferResponseForShop();
			offerResponse.setId(offer.getId());
			offerResponse.setName(offer.getName());
			offerResponse.setDescription(offer.getDescription());
			offerResponse.setExpirationDate(DateUtil.getUnixTimeStampFromDate(offer.getExpirationDate()));
			offerResponse.setImageUrl(offer.getImageUrl());
			offerResponse.setStock(offer.getStock());
			offerResponse.setShop(offer.getShop());
			offerResponse.setOfferType(offerTypeResponse);
			offerResponse.setPaymentMethod(paymentMethodResponse);
			offerResponse.setLqnLoyaltyPoints(offer.getLqnLoyaltyPoints());
			offerResponse.setRefundMoneyPercentage(offer.getRefundMoneyPercentage());
			offerResponse.setEnabled(offer.getEnabled());
			offerResponse.setLqnScore(offer.getLqnScore());
			offerResponse.setAmountReserv(offer.getAmountReserv());
			offerResponseList.add(offerResponse);
		}
		return offerResponseList;
	}

	public List<OfferResponseForUser> getOfferResponseForUserListFromOffersUser(List<OfferUser> offerList){
		List<OfferResponseForUser> offerResponseList = new ArrayList<>();
		for(OfferUser offerUser : offerList){

			OfferTypeResponse offerTypeResponse = new OfferTypeResponse();
			offerTypeResponse.setId(offerUser.getOffer().getOfferType().getId());
			offerTypeResponse.setActualPrice(offerUser.getOffer().getActualPrice());
			offerTypeResponse.setPreviousPrice(offerUser.getOffer().getPreviousPrice());
			offerTypeResponse.setDiscountPercentage(offerUser.getOffer().getDiscountPercentage());
			offerTypeResponse.setComboType(offerUser.getOffer().getComboType());

			PaymentMethodResponse paymentMethodResponse = new PaymentMethodResponse();
			paymentMethodResponse.setId(offerUser.getOffer().getPaymentMethod().getId());
			paymentMethodResponse.setCreditCards(offerUser.getOffer().getCreditCards());

			OfferResponseForUser offerResponse = new OfferResponseForUser();
			offerResponse.setId(offerUser.getOffer().getId());
			offerResponse.setName(offerUser.getOffer().getName());
			offerResponse.setDescription(offerUser.getOffer().getDescription());
			offerResponse.setExpirationDate(DateUtil.getUnixTimeStampFromDate(offerUser.getOffer().getExpirationDate()));
			offerResponse.setImageUrl(offerUser.getOffer().getImageUrl());
			offerResponse.setStock(offerUser.getOffer().getStock());
			offerResponse.setShop(offerUser.getOffer().getShop());
			offerResponse.setOfferType(offerTypeResponse);
			offerResponse.setPaymentMethod(paymentMethodResponse);
			offerResponse.setLqnLoyaltyPoints(offerUser.getOffer().getLqnLoyaltyPoints());
			offerResponse.setRefundMoneyPercentage(offerUser.getOffer().getRefundMoneyPercentage());
			offerResponse.setEnabled(offerUser.getOffer().getEnabled());
			offerResponse.setLqnScore(offerUser.getOffer().getLqnScore());
			offerResponse.setHashCode(offerUser.getHashCode());
			offerResponseList.add(offerResponse);
		}
		return offerResponseList;
	}

	public OfferResponse getOfferResponseFromOffer(Offer offer){
		OfferTypeResponse offerTypeResponse = new OfferTypeResponse();
		offerTypeResponse.setId(offer.getOfferType().getId());
		offerTypeResponse.setActualPrice(offer.getActualPrice());
		offerTypeResponse.setPreviousPrice(offer.getPreviousPrice());
		offerTypeResponse.setDiscountPercentage(offer.getDiscountPercentage());
		offerTypeResponse.setComboType(offer.getComboType());

		PaymentMethodResponse paymentMethodResponse = new PaymentMethodResponse();
		paymentMethodResponse.setId(offer.getPaymentMethod().getId());
		paymentMethodResponse.setCreditCards(offer.getCreditCards());

		OfferResponse offerResponse = new OfferResponse();
		offerResponse.setId(offer.getId());
		offerResponse.setName(offer.getName());
		offerResponse.setDescription(offer.getDescription());
		offerResponse.setExpirationDate(DateUtil.getUnixTimeStampFromDate(offer.getExpirationDate()));
		offerResponse.setImageUrl(offer.getImageUrl());
		offerResponse.setStock(offer.getStock());
		offerResponse.setShop(offer.getShop());
		offerResponse.setOfferType(offerTypeResponse);
		offerResponse.setPaymentMethod(paymentMethodResponse);
		offerResponse.setLqnLoyaltyPoints(offer.getLqnLoyaltyPoints());
		offerResponse.setRefundMoneyPercentage(offer.getRefundMoneyPercentage());
		offerResponse.setEnabled(offer.getEnabled());
		offerResponse.setLqnScore(offer.getLqnScore());

		return offerResponse;
	}
	
	public void insertOffer(OfferRequest or) {
		Offer offer = new Offer();
		offer.setName(or.getName());
		offer.setDescription(or.getDescription());
		offer.setExpirationDate(DateUtil.getDateFromUnixTimeStamp(or.getExpirationDate()));
		offer.setImageUrl(or.getImageUrl());
		offer.setStock(or.getStock());
		offer.setShop(shopService.findById(or.getShopId()));
		offer.setOfferType(offerTypeService.findById(or.getOfferTypeId()));
		offer.setPaymentMethod(paymentMethodService.findById(or.getPaymentMethodId()));
		offer.setLqnLoyaltyPoints(or.getLqnLoyaltyPoints());
		offer.setRefundMoneyPercentage(or.getRefundMoneyPercentage());
		offer.setActualPrice(or.getActualPrice());
		offer.setPreviousPrice(or.getPreviousPrice());
		offer.setEnabled(or.getEnabled());
		offer.setDiscountPercentage(or.getDiscountPercentage());

		this.persist(offer);
	}

	public void updateOffer(OfferRequest or){
		Offer offer = this.findById(or.getId());
		if(or.getName() != null)
			offer.setName(or.getName());
		if(or.getDescription() != null)
			offer.setDescription(or.getDescription());
		if(or.getExpirationDate() != null)
			offer.setExpirationDate(DateUtil.getDateFromUnixTimeStamp(or.getExpirationDate()));
		if(or.getImageUrl() != null)
			offer.setImageUrl(or.getImageUrl());
		if(or.getStock() != null)
			offer.setStock(or.getStock());
		if(or.getShopId() != null)
			offer.setShop(shopService.findById(or.getShopId()));
		if(or.getOfferTypeId() != null)
			offer.setOfferType(offerTypeService.findById(or.getOfferTypeId()));
		if(or.getPaymentMethodId() != null)
			offer.setPaymentMethod(paymentMethodService.findById(or.getPaymentMethodId()));
		if(or.getLqnLoyaltyPoints() != null)
			offer.setLqnLoyaltyPoints(or.getLqnLoyaltyPoints());
		if(or.getRefundMoneyPercentage() != null)
			offer.setRefundMoneyPercentage(or.getRefundMoneyPercentage());
		if(or.getActualPrice() != null)
			offer.setActualPrice(or.getActualPrice());
		if(or.getPreviousPrice() != null)
			offer.setPreviousPrice(or.getPreviousPrice());
		if(or.getDiscountPercentage() != null)
			offer.setDiscountPercentage(or.getDiscountPercentage());
		if(or.getEnabled() != null)
			offer.setEnabled(or.getEnabled());

		this.update(offer);
	}
}
