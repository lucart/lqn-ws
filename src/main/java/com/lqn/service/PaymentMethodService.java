package com.lqn.service;

import com.lqn.dao.PaymentMethodDaoImpl;
import com.lqn.model.PaymentMethod;

public class PaymentMethodService {
	private static PaymentMethodDaoImpl paymentMethodDao;

	public PaymentMethodService() {
		paymentMethodDao = new PaymentMethodDaoImpl();
	}

	public PaymentMethod findById(Integer id) {
		PaymentMethod paymentMethod = paymentMethodDao.findById(id);
		return paymentMethod;
	}
}
