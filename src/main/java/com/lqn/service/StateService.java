package com.lqn.service;

import com.lqn.dao.StateDaoImpl;
import com.lqn.model.State;

public class StateService {
    private static StateDaoImpl stateDao;

    public StateService() {
        stateDao = new StateDaoImpl();
    }

    public State findById(Integer id) {
        State state = stateDao.findById(id);
        return state;
    }
}
