package com.lqn.service;

import com.lqn.dao.UserDaoImpl;
import com.lqn.model.Offer;
import com.lqn.model.OfferUser;
import com.lqn.model.State;
import com.lqn.model.User;
import com.lqn.responses.UserResponse;
import util.HashCodeUtil;

public class UserService {
	private static UserDaoImpl userDao;
	private static OfferService offerService;
	private static StateService stateService;
	private static OfferUserService offerUserService;

	public UserService() {
		userDao = new UserDaoImpl();
		offerService = new OfferService();
		stateService = new StateService();
		offerUserService = new OfferUserService();
	}

	public User findById(Integer id) {
		User user = userDao.findById(id);
		return user;
	}

	public User findByUsername(String username) {
		User user = userDao.findByUsername(username);
		return user;
	}

	public void reserveOffer(int offerId, int userId){
		User user = this.findById(userId);
		Offer offer = offerService.findById(offerId);
		State state = stateService.findById(1);

		boolean exist;
		do {
			String hashCode = HashCodeUtil.getHashCode();
			exist = offerUserService.validateHashCode(hashCode);
		} while (exist);

		OfferUser offerUser = new OfferUser();

		offerUser.setOffer(offer);
		offerUser.setUser(user);
		offerUser.setState(state);
		offerUser.setHashCode(HashCodeUtil.getHashCode());

		offerUserService.persist(offerUser);
	}

	public UserResponse getUserResponseFromUser(User user){
		UserResponse userResponse = new UserResponse();
		userResponse.setId(user.getId());
		userResponse.setUsername(user.getUsername());
		userResponse.setUserType(user.getUserType());
		if(user.getShop() != null)
			userResponse.setShop(user.getShop());
		return userResponse;
	}

	public boolean isAsociatedToShop(int userId, int shopId) {
		return userDao.isAsociatedToShop(userId, shopId);
	}

}
