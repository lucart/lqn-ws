package com.lqn.responses;

import java.util.List;

import com.lqn.model.CreditCard;

public class PaymentMethodResponse {
	private Integer id;
	private List<CreditCard> creditCards;
	
	public PaymentMethodResponse(){
	}

	public PaymentMethodResponse(Integer id, List<CreditCard> creditCards) {
		super();
		this.id = id;
		this.creditCards = creditCards;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<CreditCard> getCreditCards() {
		return creditCards;
	}

	public void setCreditCards(List<CreditCard> creditCards) {
		this.creditCards = creditCards;
	}
}
