package com.lqn.responses;

public class GenericResponse {
	private int code;
	private String message;
	
	public GenericResponse(){
		
	}
	
	public GenericResponse(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "GenericResponse [code=" + code + ", message=" + message + "]";
	}
}
