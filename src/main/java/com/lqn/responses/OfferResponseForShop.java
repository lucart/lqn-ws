package com.lqn.responses;

import com.lqn.model.Shop;

public class OfferResponseForShop extends OfferResponse {
    Integer amountReserv;

    public OfferResponseForShop() {

    }

    public OfferResponseForShop(Integer amountReserve) {
        this.amountReserv = amountReserve;
    }

    public OfferResponseForShop(Integer id, String name, String description, Long expirationDate, String imageUrl, Integer stock, Shop shop, OfferTypeResponse offerType, PaymentMethodResponse paymentMethod, Integer lqnLoyaltyPoints, Double lqnScore, Boolean enabled, Integer refundMoneyPercentage, Integer amountReserve) {
        super(id, name, description, expirationDate, imageUrl, stock, shop, offerType, paymentMethod, lqnLoyaltyPoints, lqnScore, enabled, refundMoneyPercentage);
        this.amountReserv = amountReserve;
    }

    public Integer getAmountReserv() {
        return amountReserv;
    }

    public void setAmountReserv(Integer amountReserv) {
        this.amountReserv = amountReserv;
    }
}
