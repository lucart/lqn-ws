package com.lqn.responses;

import com.lqn.model.User;

public class OfferUserResponse {
    OfferResponse offer;
    UserResponse user;

    public OfferUserResponse(){

    }

    public OfferUserResponse(OfferResponse offer, UserResponse user) {
        this.offer = offer;
        this.user = user;
    }

    public OfferResponse getOffer() {
        return offer;
    }

    public void setOffer(OfferResponse offer) {
        this.offer = offer;
    }

    public UserResponse getUser() {
        return user;
    }

    public void setUser(UserResponse user) {
        this.user = user;
    }
}
