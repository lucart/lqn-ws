package com.lqn.responses;

import com.lqn.model.ComboType;

public class OfferTypeResponse {
	private Integer id;	
	private Integer discountPercentage;
	private Double actualPrice;
	private Double previousPrice;
	private ComboType comboType;
	
	public OfferTypeResponse(){
		
	}

	public OfferTypeResponse(Integer id, Integer discountPercentage, Double actualPrice, Double previousPrice,
							 ComboType comboType) {
		this.id = id;
		this.discountPercentage = discountPercentage;
		this.actualPrice = actualPrice;
		this.previousPrice = previousPrice;
		this.comboType = comboType;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Integer discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public Double getActualPrice() {
		return actualPrice;
	}

	public void setActualPrice(Double actualPrice) {
		this.actualPrice = actualPrice;
	}

	public Double getPreviousPrice() {
		return previousPrice;
	}

	public void setPreviousPrice(Double previousPrice) {
		this.previousPrice = previousPrice;
	}

	public ComboType getComboType() {
		return comboType;
	}

	public void setComboType(ComboType comboType) {
		this.comboType = comboType;
	}
}
