package com.lqn.responses;

import java.util.Date;
import com.lqn.model.Shop;

import util.DateUtil;

public class OfferResponse {
	private Integer id;	
	private String name;
	private String description;
	private Long expirationDate;
	private String imageUrl;
	private Integer stock;
	private Shop shop;
	private OfferTypeResponse offerType;
	private PaymentMethodResponse paymentMethod;
	private Integer lqnLoyaltyPoints;
	private Double lqnScore;
	private Boolean enabled;
    private Integer refundMoneyPercentage;
	
	public OfferResponse(){
		
	}

	public OfferResponse(Integer id, String name, String description, Long expirationDate, String imageUrl, Integer stock, Shop shop, OfferTypeResponse offerType, PaymentMethodResponse paymentMethod, Integer lqnLoyaltyPoints, Double lqnScore, Boolean enabled, Integer refundMoneyPercentage) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.expirationDate = expirationDate;
		this.imageUrl = imageUrl;
		this.stock = stock;
		this.shop = shop;
		this.offerType = offerType;
		this.paymentMethod = paymentMethod;
		this.lqnLoyaltyPoints = lqnLoyaltyPoints;
		this.lqnScore = lqnScore;
		this.enabled = enabled;
		this.refundMoneyPercentage = refundMoneyPercentage;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Long expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public OfferTypeResponse getOfferType() {
		return offerType;
	}

	public void setOfferType(OfferTypeResponse offerType) {
		this.offerType = offerType;
	}

	public PaymentMethodResponse getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethodResponse paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Integer getLqnLoyaltyPoints() {
		return lqnLoyaltyPoints;
	}

	public void setLqnLoyaltyPoints(Integer lqnLoyaltyPoints) {
		this.lqnLoyaltyPoints = lqnLoyaltyPoints;
	}

	public Double getLqnScore() {
		return lqnScore;
	}

	public void setLqnScore(Double lqnScore) {
		this.lqnScore = lqnScore;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Integer getRefundMoneyPercentage() {
		return refundMoneyPercentage;
	}

	public void setRefundMoneyPercentage(Integer refundMoneyPercentage) {
		this.refundMoneyPercentage = refundMoneyPercentage;
	}
}
