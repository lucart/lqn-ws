package com.lqn.responses;

import com.lqn.model.Shop;

public class OfferResponseForUser extends OfferResponse{
    String hashCode;

    public OfferResponseForUser() {

    }

    public OfferResponseForUser(String hashCode) {
        this.hashCode = hashCode;
    }

    public OfferResponseForUser(Integer id, String name, String description, Long expirationDate, String imageUrl, Integer stock, Shop shop, OfferTypeResponse offerType, PaymentMethodResponse paymentMethod, Integer lqnLoyaltyPoints, Double lqnScore, Boolean enabled, Integer refundMoneyPercentage, String hashCode) {
        super(id, name, description, expirationDate, imageUrl, stock, shop, offerType, paymentMethod, lqnLoyaltyPoints, lqnScore, enabled, refundMoneyPercentage);
        this.hashCode = hashCode;
    }

    public String getHashCode() {
        return hashCode;
    }

    public void setHashCode(String hashCode) {
        this.hashCode = hashCode;
    }
}
