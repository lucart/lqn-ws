package com.lqn.responses;

import com.lqn.model.Shop;
import com.lqn.model.UserType;

public class UserResponse {
    private Integer id;
    private String username;
    private UserType userType;
    private Shop shop;

    public UserResponse(){

    }

    public UserResponse(Integer id, String username, UserType userType, Shop shop) {
        this.id = id;
        this.username = username;
        this.userType = userType;
        this.shop = shop;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }
}
