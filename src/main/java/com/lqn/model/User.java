package com.lqn.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;

@Entity
@Table(name = "user")
public class User {
	@Id
	@Column(name = "id")
	private Integer id;	
	@Column(name = "username")
	private String username;
	@Column(name = "password")
	private String password;
	@ManyToOne
	@JoinColumn(name = "user_type_id")
	private UserType userType;
	@ManyToOne
	@JoinColumn(name = "shop_id")
	private Shop shop;

	@OneToMany(mappedBy = "user")
	private List<OfferUser> offerUsers;
	
	public User(){
		
	}

	public User(Integer id, String username, String password, UserType userType, Shop shop, List<OfferUser> offerUsers) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.userType = userType;
		this.shop = shop;
		this.offerUsers = offerUsers;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public List<OfferUser> getOfferUsers() {
		return offerUsers;
	}

	public void setOfferUsers(List<OfferUser> offerUsers) {
		this.offerUsers = offerUsers;
	}
}
