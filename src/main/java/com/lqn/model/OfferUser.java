package com.lqn.model;

import javax.persistence.*;

@Entity
@Table(name = "offer_user")
public class OfferUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user")
    private User user;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "offer")
    private Offer offer;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "state")
    private State state;
    @Column(name = "hash_code")
    private String hashCode;

    public OfferUser(){

    }

    public OfferUser(Integer id, User user, Offer offer, State state, String hashCode) {
        this.id = id;
        this.user = user;
        this.offer = offer;
        this.state = state;
        this.hashCode = hashCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getHashCode() {
        return hashCode;
    }

    public void setHashCode(String hashCode) {
        this.hashCode = hashCode;
    }
}
