package com.lqn.model;

import org.hibernate.annotations.Formula;

import java.util.Date;
import java.util.List;

import javax.persistence.*;


@Entity
@Table(name = "offer")
public class Offer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;	
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "expiration_date")	
	private Date expirationDate;
	
	@Column(name = "image_url")
	private String imageUrl;
	
	@Column(name = "stock")
	private Integer stock;
	
	@ManyToOne
	@JoinColumn(name = "shop_id")
	private Shop shop;
	
	@ManyToOne
	@JoinColumn(name = "offer_type_id")
	private OfferType offerType;

	@ManyToOne
	@JoinColumn(name = "payment_method_id")
	private PaymentMethod paymentMethod;
	
	@Column(name = "lqn_loyalty_points")
	private Integer lqnLoyaltyPoints;
	
	@Column(name = "refund_money_per")
	private Integer refundMoneyPercentage;
	
	@Column(name = "actual_price")
	private Double actualPrice;
	
	@Column(name = "previous_price")
	private Double previousPrice;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "offer_credit_card", joinColumns = {
			@JoinColumn(name = "offer_id", nullable = false, updatable = false) },
			inverseJoinColumns = { @JoinColumn(name = "credit_card_id",
					nullable = false, updatable = false) })
	private List<CreditCard> creditCards;

	@OneToMany(mappedBy = "user")
	private List<OfferUser> offerUsers;

	@Formula("getScore(id)")
	private Double lqnScore;
	
	@ManyToOne
	@JoinColumn(name = "combo_type_id")
	private ComboType comboType;
	
	@Column(name = "enabled", nullable = false, columnDefinition = "TINYINT(1)")
	private Boolean enabled;

	@Column(name = "discount_per")
	private Integer discountPercentage;

	@Formula("getAmountReserv(id)")
	private Integer amountReserv;
	
	public Offer(){
		
	}

	public Offer(String name, String description, Date expirationDate, String imageUrl, Integer stock, Shop shop, OfferType offerType, PaymentMethod paymentMethod, Integer lqnLoyaltyPoints, Integer refundMoneyPercentage, Double actualPrice, Double previousPrice, List<CreditCard> creditCards, List<OfferUser> offerUsers, Double lqnScore, ComboType comboType, Boolean enabled, Integer discountPercentage, Integer amountReserv) {
		this.name = name;
		this.description = description;
		this.expirationDate = expirationDate;
		this.imageUrl = imageUrl;
		this.stock = stock;
		this.shop = shop;
		this.offerType = offerType;
		this.paymentMethod = paymentMethod;
		this.lqnLoyaltyPoints = lqnLoyaltyPoints;
		this.refundMoneyPercentage = refundMoneyPercentage;
		this.actualPrice = actualPrice;
		this.previousPrice = previousPrice;
		this.creditCards = creditCards;
		this.offerUsers = offerUsers;
		this.lqnScore = lqnScore;
		this.comboType = comboType;
		this.enabled = enabled;
		this.discountPercentage = discountPercentage;
		this.amountReserv = amountReserv;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	public OfferType getOfferType() {
		return offerType;
	}

	public void setOfferType(OfferType offerType) {
		this.offerType = offerType;
	}

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Integer getLqnLoyaltyPoints() {
		return lqnLoyaltyPoints;
	}

	public void setLqnLoyaltyPoints(Integer lqnLoyaltyPoints) {
		this.lqnLoyaltyPoints = lqnLoyaltyPoints;
	}

	public Integer getRefundMoneyPercentage() {
		return refundMoneyPercentage;
	}

	public void setRefundMoneyPercentage(Integer refundMoneyPercentage) {
		this.refundMoneyPercentage = refundMoneyPercentage;
	}

	public Double getActualPrice() {
		return actualPrice;
	}

	public void setActualPrice(Double actualPrice) {
		this.actualPrice = actualPrice;
	}

	public Double getPreviousPrice() {
		return previousPrice;
	}

	public void setPreviousPrice(Double previousPrice) {
		this.previousPrice = previousPrice;
	}

	public List<CreditCard> getCreditCards() {
		return creditCards;
	}

	public void setCreditCards(List<CreditCard> creditCards) {
		this.creditCards = creditCards;
	}

	public List<OfferUser> getOfferUsers() {
		return offerUsers;
	}

	public void setOfferUsers(List<OfferUser> offerUsers) {
		this.offerUsers = offerUsers;
	}

	public Double getLqnScore() {
		return lqnScore;
	}

	public void setLqnScore(Double lqnScore) {
		this.lqnScore = lqnScore;
	}

	public ComboType getComboType() {
		return comboType;
	}

	public void setComboType(ComboType comboType) {
		this.comboType = comboType;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Integer getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Integer discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public Integer getAmountReserv() {
		return amountReserv;
	}

	public void setAmountReserv(Integer amountReserv) {
		this.amountReserv = amountReserv;
	}
}
