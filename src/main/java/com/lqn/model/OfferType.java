package com.lqn.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "offer_type")
public class OfferType {
	@Id
	@Column(name = "id")
	private Integer id;	
	@Column(name = "name")
	private String name;
	@Column(name = "description")
	private String description;
	
	public OfferType(){
		
	}

	public OfferType(Integer id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "OfferType [id=" + id + ", name=" + name + ", description=" + description + "]";
	}

	
}
