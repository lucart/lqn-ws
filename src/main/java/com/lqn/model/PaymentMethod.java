package com.lqn.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "payment_method")
public class PaymentMethod {
	@Id
	@Column(name = "id")
	private Integer id;	
	@Column(name = "name")
	private String name;
	@Column(name = "description")
	private String description;
	
	public PaymentMethod(){
		
	}
	
	public PaymentMethod(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "PaymentMethod [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
}
