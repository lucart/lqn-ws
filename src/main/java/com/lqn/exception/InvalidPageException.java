package com.lqn.exception;

@SuppressWarnings("serial")
public class InvalidPageException extends Exception{
	
	public InvalidPageException(String message) {
        super(message);
    }

}
