package com.lqn.dao;

import java.util.List;

import com.lqn.model.*;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.lqn.exception.InvalidPageException;

public class OfferDaoImpl extends GenericDaoImpl<Offer,Integer> implements OfferDao {

    public OfferDaoImpl() {
    }
	
	@SuppressWarnings("unchecked")
	public List<Offer> findByOfferTypeId(int page, int offerTypeId) throws InvalidPageException{
		int pageSize = 10;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria criteria = session.createCriteria(Offer.class);
		criteria.createAlias("offerType", "ot");
		criteria.add(Restrictions.eq("ot.id", offerTypeId));
		criteria.add(Restrictions.eq("enabled", true));
        criteria.setFirstResult((page - 1) * pageSize);
        criteria.setMaxResults(pageSize);
		criteria.addOrder(Order.desc("lqnScore"));
        
        List<Offer> offers = (List<Offer>) criteria.list();
		session.close();
        if(offers.isEmpty()){
        	throw new InvalidPageException("Invalid Page Number");
        }else{
        	return offers;
        }
	}

	@SuppressWarnings("unchecked")
	public List<Offer> searchOffers(int page, String key) throws InvalidPageException{
		int pageSize = 10;
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria criteria = session.createCriteria(Offer.class);
		criteria.add(Restrictions.eq("enabled", true));
		criteria.add(Restrictions.disjunction()
            .add(Restrictions.like("name", "%" + key + "%"))
            .add(Restrictions.like("description", "%" + key + "%")));
		criteria.setFirstResult((page - 1) * pageSize);
		criteria.setMaxResults(pageSize);
		criteria.addOrder(Order.desc("lqnScore"));

		List<Offer> offers = (List<Offer>) criteria.list();
		session.close();
		if(offers.isEmpty()){
			throw new InvalidPageException("Invalid Page Number");
		}else{
			return offers;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Offer> findOffersByShopId(int shopId) throws InvalidPageException{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria criteria = session.createCriteria(Offer.class);
		criteria.createAlias("shop", "sh");
		criteria.add(Restrictions.eq("sh.id", shopId));
		criteria.addOrder(Order.desc("id"));
        
        List<Offer> offers = (List<Offer>) criteria.list();
        session.close();
        if(offers.isEmpty()){
        	throw new InvalidPageException("No Offers for the given shop id: '" + shopId + "'");
        }else{
        	return offers;
        }
	}
}
