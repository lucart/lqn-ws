package com.lqn.dao;

import com.lqn.model.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        Configuration configuration = new Configuration().configure();
        configuration.addAnnotatedClass(Offer.class);
        configuration.addAnnotatedClass(OfferType.class);
        configuration.addAnnotatedClass(PaymentMethod.class);
        configuration.addAnnotatedClass(Shop.class);
        configuration.addAnnotatedClass(CreditCard.class);
        configuration.addAnnotatedClass(ComboType.class);
        configuration.addAnnotatedClass(User.class);
        configuration.addAnnotatedClass(UserType.class);
        configuration.addAnnotatedClass(OfferUser.class);
        configuration.addAnnotatedClass(State.class);
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties());
        SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
        return sessionFactory;
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

}