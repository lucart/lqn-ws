package com.lqn.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.lqn.model.User;

public class UserDaoImpl extends GenericDaoImpl<User,Integer> implements UserDao {

	public UserDaoImpl() {
	}
	
	public User findByUsername(String username) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria criteria = session.createCriteria(User.class);
		User user = (User) criteria.add(Restrictions.eq("username", username))
		                             .uniqueResult();
		session.close();
		return user;
	}

	public boolean isAsociatedToShop(int userId, int shopId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("id", userId));
		criteria.add(Restrictions.eq("shop.id", shopId));
		User user = (User) criteria.uniqueResult();
		session.close();

		if(user == null){
			return false;
		} else {
			return true;
		}
	}
}

