package com.lqn.dao;

import java.util.List;

import com.lqn.model.OfferUser;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class OfferUserDaoImpl  extends GenericDaoImpl<OfferUser,Integer> implements OfferUserDao {

    public OfferUserDaoImpl() {
    }

    public OfferUser findByHashCode(String hashCode) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(OfferUser.class);
        OfferUser user = (OfferUser) criteria.add(Restrictions.eq("hashCode", hashCode))
                .uniqueResult();
        session.close();
        return user;
    }

    public List<OfferUser> findByUserId(int userId) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(OfferUser.class);
        List<OfferUser> offers = (List<OfferUser>) criteria.add(Restrictions.eq("user.id", userId))
                .list();
        session.close();
        return offers;
    }

    public boolean existHashCode(String hashCode) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(OfferUser.class);
        criteria.add(Restrictions.eq("hashCode", hashCode));
        criteria.setProjection(Projections.rowCount());
        long count = (Long) criteria.uniqueResult();
        return count != 0;
    }
}
