package com.lqn.dao;

import java.io.Serializable;
import java.util.List;

import com.lqn.model.User;

public interface UserDao<T, Id extends Serializable>{
	User findByUsername(String username);
}

