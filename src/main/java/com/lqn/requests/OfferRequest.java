package com.lqn.requests;

public class OfferRequest {
	private Integer id;	
	private String name;
	private String description;
	private Long expirationDate;
	private String imageUrl;
	private Integer stock;
	private Integer shopId;
	private Integer offerTypeId;
	private Integer paymentMethodId;
	private Integer lqnLoyaltyPoints;
	private Integer refundMoneyPercentage;
	private Double actualPrice;
	private Double previousPrice;
	private Boolean enabled;
	private Integer discountPercentage;
	
	public OfferRequest(){
		
	}

	public OfferRequest(Integer id, String name, String description, Long expirationDate, String imageUrl,
			Integer stock, Integer shopId, Integer offerTypeId, Integer paymentMethodId, Integer lqnLoyaltyPoints,
			Integer refundMoneyPercentage, Double actualPrice, Double previousPrice, Boolean enabled, Integer discountPercentage) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.expirationDate = expirationDate;
		this.imageUrl = imageUrl;
		this.stock = stock;
		this.shopId = shopId;
		this.offerTypeId = offerTypeId;
		this.paymentMethodId = paymentMethodId;
		this.lqnLoyaltyPoints = lqnLoyaltyPoints;
		this.refundMoneyPercentage = refundMoneyPercentage;
		this.actualPrice = actualPrice;
		this.previousPrice = previousPrice;
		this.enabled = enabled;
		this.discountPercentage = discountPercentage;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Long expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public Integer getShopId() {
		return shopId;
	}

	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}

	public Integer getOfferTypeId() {
		return offerTypeId;
	}

	public void setOfferTypeId(Integer offerTypeId) {
		this.offerTypeId = offerTypeId;
	}

	public Integer getPaymentMethodId() {
		return paymentMethodId;
	}

	public void setPaymentMethodId(Integer paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public Integer getLqnLoyaltyPoints() {
		return lqnLoyaltyPoints;
	}

	public void setLqnLoyaltyPoints(Integer lqnLoyaltyPoints) {
		this.lqnLoyaltyPoints = lqnLoyaltyPoints;
	}

	public Integer getRefundMoneyPercentage() {
		return refundMoneyPercentage;
	}

	public void setRefundMoneyPercentage(Integer refundMoneyPercentage) {
		this.refundMoneyPercentage = refundMoneyPercentage;
	}

	public Double getActualPrice() {
		return actualPrice;
	}

	public void setActualPrice(Double actualPrice) {
		this.actualPrice = actualPrice;
	}

	public Double getPreviousPrice() {
		return previousPrice;
	}

	public void setPreviousPrice(Double previousPrice) {
		this.previousPrice = previousPrice;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Integer getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Integer discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	@Override
	public String toString() {
		return "OfferRequest{" +
				"id=" + id +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", expirationDate=" + expirationDate +
				", imageUrl='" + imageUrl + '\'' +
				", stock=" + stock +
				", shopId=" + shopId +
				", offerTypeId=" + offerTypeId +
				", paymentMethodId=" + paymentMethodId +
				", lqnLoyaltyPoints=" + lqnLoyaltyPoints +
				", refundMoneyPercentage=" + refundMoneyPercentage +
				", actualPrice=" + actualPrice +
				", previousPrice=" + previousPrice +
				", enabled=" + enabled +
				", discountPercentage=" + discountPercentage +
				'}';
	}
}
